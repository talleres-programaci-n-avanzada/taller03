#include <iostream>
#include <iomanip>
#include <fstream>
#include <string>
#include <sstream>
#include <cstring>

using namespace std;

const int CODIGO_ERROR= -1;
const int CODIGO_EXITO = 0;

struct herramienta{
    short registro;
    char nombre[100];
    short cantidad;
    int costo;
};

struct listaHerramientas{
    int tam;
    herramienta herram[100];
};

bool leerTexto(string inputFileName, listaHerramientas &herramientas, char DELIMITADOR);
bool escribirBinario(string outputFileName, listaHerramientas &herramientas);
void imprimirHerramienta(herramienta herramAux);
void listar(listaHerramientas &herramientas);
void ordenarBinario(string outputFileName, listaHerramientas &herramientas);
void nueva_herramienta(string outputFileName, listaHerramientas &herramientas);
bool buscar_herramienta(string fileName, int pos, herramienta &auxHerr);
void editar_herramienta(herramienta &auxHerr, listaHerramientas &herramientas);
void eliminar_herramienta(herramienta &auxHerr, listaHerramientas &herramientas);

int main(void){
    string input_fileName = "herram.txt";
    string output_fileName = "herram.bin";
    listaHerramientas herramientas;
    const char DELIMITADOR = ' ';
    short opcion=0;
    
    while(opcion!=7){
        cout << "Bienvenido Manny, listo para ponerte a la obra? Selecciona la opción que desees: \n";
        cout << "1. Capturar datos iniciales en un archivo binario de las herramientas desde un archivo de tipo texto. \n";
        cout << "2. Listar las herramientas. \n";
        cout << "3. Ordenar el archivo binario por # de registro \n";
        cout << "4. Insertar al final una herramienta de forma manual en el lugar correcto. \n";
        cout << "5. Modificar los datos de una herramienta dado el # de registro. \n";
        cout << "6. Eliminar una herramienta y actualizar archivo. \n";
        cout << "7. Salir. \n";
        cin >> opcion;
        if(opcion==1){
            bool exito_lectura_texto = leerTexto(input_fileName, herramientas, DELIMITADOR);
            if(!exito_lectura_texto) {
                cout << "Error al leer el archivo de texto " << input_fileName << endl;
                return CODIGO_ERROR;
            }
            cout << "Éxito leyendo archivo de texto!" << endl;
            bool exito_escritura_binario = escribirBinario(output_fileName, herramientas);
            if(!exito_escritura_binario) {
                cout << "Error al escribir el archivo binario " << output_fileName << endl;
                return CODIGO_ERROR;
            }
            cout << "Éxito escribiendo archivo binario!" << endl;
        }
        if(opcion==2){
            listar(herramientas);
        }
        if(opcion==3){
            ordenarBinario(output_fileName, herramientas);            
        }
        if(opcion==4){
            nueva_herramienta(output_fileName, herramientas);
        }
        if(opcion==5){
            int pos;
            cout << "Ingrese el ID de la herramienta que desea modificar: ";
            cin >> pos;
            herramienta auxHerr, auxHerr2;                 
            bool herramientaEncontrada = false;
            for (int i = 0; i < herramientas.tam; i++) {
                auxHerr2 = herramientas.herram[i];
                if (auxHerr2.registro == pos) {
                    auxHerr = auxHerr2;
                    herramientaEncontrada = true;
                    break;
                }
            }
            if (herramientaEncontrada) {
                editar_herramienta(auxHerr, herramientas);
                ordenarBinario(output_fileName, herramientas);
            } 
            else {
                cout << "Herramienta no encontrada." << endl;
            }
        }
        if(opcion==6){
            int pos;
            cout << "Ingrese el ID de la herramienta que desea eliminar: ";
            cin >> pos;
            herramienta auxHerr, auxHerr2;
            bool herramientaEncontrada = false;
            for (int i = 0; i < herramientas.tam; i++) {
                auxHerr2 = herramientas.herram[i];
                if (auxHerr2.registro == pos) {
                    auxHerr = auxHerr2;
                    herramientaEncontrada = true;
                    break;
                }
            }
            if (herramientaEncontrada) {
                eliminar_herramienta(auxHerr, herramientas);
                ordenarBinario(output_fileName, herramientas);
            }
            else {
                cout << "Herramienta no encontrada." << endl;
            }
        }
    }
}

bool leerTexto(string inputFileName, listaHerramientas &herramientas, char DELIMITADOR){
    fstream file;
    file.open(inputFileName, ios::in);
    string line;
    int i = 0;
    string word;
    herramienta herr_aux;
    if(!file.is_open()){
        return false;
    }
    while(getline(file,line)) {
        stringstream str(line);
        getline(str, word, DELIMITADOR);
        herr_aux.registro = stoi(word);
        getline(str, word, DELIMITADOR);
        strcpy(herr_aux.nombre, word.c_str());
        getline(str, word, DELIMITADOR);
        herr_aux.cantidad = stoi(word);
        getline(str, word, DELIMITADOR);
        herr_aux.costo = stoi(word);
        herramientas.herram[i]=herr_aux;
        i++;
    }
    file.close();
    herramientas.tam=i;
    return true;
}

bool escribirBinario(string outputFileName, listaHerramientas &herramientas){
    fstream file;
    file.open(outputFileName, ios::out | ios::binary);
    int i = 0;
    herramienta herr_aux;
    if(!file.is_open()) {
        return false;
    }
    for(int i = 0; i < herramientas.tam; i++){
        herr_aux = herramientas.herram[i];
        file.write((char*)&herr_aux, sizeof(herr_aux));
    }
    file.close();
    return true;
}

void imprimirHerramienta(herramienta herramAux){
    cout <<setw(7)<<herramAux.registro <<setw(30)<<herramAux.nombre <<setw(11)<<herramAux.cantidad <<setw(17)<<herramAux.costo<<"\n";
}

void listar(listaHerramientas &herramientas){
    cout << "ID registro:  |      Nombre:      |   Cantidad:   |   Costo:   \n";
    herramienta herram_aux;
    for(int i=0; i<herramientas.tam; i++){
        herram_aux = herramientas.herram[i];
        imprimirHerramienta(herram_aux);
    }
}

void ordenarBinario(string outputFileName, listaHerramientas &herramientas){
    herramienta herr[100], aux;
    for(int i=0; i<herramientas.tam; i++){
        herr[i]=herramientas.herram[i];
    }
    for(int i=0; i<herramientas.tam; i++){
        int min=i;
        for(int j=i+1; j<herramientas.tam; j++){
            if(herr[j].registro<herr[min].registro){
                min=j;
            }
        }
        aux=herr[i];
        herr[i]=herr[min];
        herr[min]=aux;
    }
    for(int i=0; i<herramientas.tam; i++){
        herramientas.herram[i]=herr[i];
    }
    escribirBinario(outputFileName, herramientas);
}

void nueva_herramienta(string outputFileName, listaHerramientas &herramientas){
    herramienta nueva;
    int conteo=herramientas.tam;
    cout<<"Ingrese el ID de la nueva herramienta, revise que no se repita: \n";
    cin >> nueva.registro;
    cout<< "Ingrese el nombre de esta: \n";
    cin >> nueva.nombre;
    cout<< "Ingrese la cantidad: \n";
    cin >> nueva.cantidad;
    cout<< "Ingrese el costo unitario: \n";
    cin >> nueva.costo;
    herramientas.herram[conteo]=nueva;
    herramientas.tam++;
    herramienta herramAux;
    ordenarBinario(outputFileName, herramientas);
}

bool buscar_herramienta(string fileName, int pos, herramienta &auxHerr){
    fstream file;
    file.open(fileName, ios::in | ios::binary);
    int pos_archivo_binario = pos * sizeof(auxHerr);
    if(!file.is_open()) {
        return false;
    }
    file.seekg(pos_archivo_binario);
    file.read((char*)&auxHerr, sizeof(auxHerr));
    file.close();
    return true;
}

void editar_herramienta(herramienta &auxHerr, listaHerramientas &herramientas) {
    int op = 0;
    while (op != 4) {
        cout << "Seleccione lo que desee modificar de la herramienta (no es posible el ID): \n";
        cout << "1. Nombre. (" << auxHerr.nombre << ")\n";
        cout << "2. Cantidad. (" << auxHerr.cantidad << ")\n";
        cout << "3. Costo unitario. (" << auxHerr.costo << ")\n";
        cout << "4. Salir.\n";
        cin >> op;
        if (op == 1) {
            cout << "Ingrese nuevo nombre: ";
            cin >> auxHerr.nombre;
        }
        if (op == 2) {
            cout << "Ingrese nueva cantidad: ";
            cin >> auxHerr.cantidad;
        }
        if (op == 3) {
            cout << "Ingrese nuevo costo: ";
            cin >> auxHerr.costo;
        }
    }
    for (int i = 0; i < herramientas.tam; i++) {
        if (herramientas.herram[i].registro == auxHerr.registro) {
            herramientas.herram[i] = auxHerr;
            break;
        }
    }
}

void eliminar_herramienta(herramienta &auxHerr, listaHerramientas &herramientas){
    for(int i=0; i<herramientas.tam; i++){
        if(herramientas.herram[i].registro == auxHerr.registro){
            herramientas.tam--;
            for(int j=i; j<herramientas.tam; j++){
                herramientas.herram[j]=herramientas.herram[j+1];
            }
        }
    }
}