# Taller03

Usted es dueño de una tienda de herramientas y necesita mantener un inventario que le pueda decir cuáles herramientas tiene, cuantas tiene y el costo de cada una. Escriba un programa que a partir del archivo “herram.txt” suba los datos al programa relacionados con algunas herramientas, y cree el archivo binario “herram.bin”, que le permita listar todas sus herramientas, que le permita eliminar un registro de una herramienta que ya no tiene y que le permita modificar la información de alguna herramienta. El número de identificación de cada herramienta debe ser su número de registro.

De igual manera usted desea generar un archivo “sal.txt” con las herramientas cuyo costototal sea superior a $100000, en el archivo debe  quedar  el  #  de  registro,  Nombre  de  la herramienta y el costoTotal, Ordenado ascendente mente por el #deregistro.

Ordene el archivo “herram.bin” de tal manera que el archivo quede ordenado por # de registro ascendentemente.
Cuando se inserta los datos de una herramienta de manera manual (digitada por el usuario) el archivo “herram.bin”, este debe quedar en el lugar correcto dentro del archivo. (El archivo ya se encuentra ordenado). 

Utilice la siguiente información que tiene usted almacenada en su disco duro para subir al programa:

```text
17 Lijadora_Eléctrica 7 130000
3 Martillo 76 35000
56 Serrucho 21 38000
39 Podadora 3 230000
24 Sierra 18 310000
68 Destornillador 106 8000
83 Mazo 11 12000
77 Llave_Inglesa 34 15000
```

## Condiciones que debe cumplir el programa
Se debe realizar un menú con las diferentes opciones:
```text
1. Capturar datos iniciales en un archivo binario de las herramientas desde un archivo de tipo texto. 
2. Listar las herramientas.
3. Ordenar el archivo binario por # de registro
4. Insertar al final una herramienta de forma manual en el lugar correcto. 
5. Modificar los datos de una herramienta dado el # de registro. 
6. Eliminar una herramienta y actualizar archivo.
```